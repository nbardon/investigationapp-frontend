import { connected } from './Stores.js';
const server_addr = `ws://${document.location.hostname}:5193`;
let ws = null;


const Proto = {
	/* WebSocket send ASCII (7bits) : Max 7F */

	// From client (00-3F)
	MSG_TYPE_AUTH:				0x00,
	MSG_TYPE_POSITION:			0x01,
	MSG_TYPE_PROGRESS_CLAIM:	0x10,
	MSG_TYPE_WAYPOINT_UPDATE:	0x1A,
	MSG_TYPE_MESSAGE:			0x20,
	MSG_TYPE_NOTE_CREATE:		0x2A,
	MSG_TYPE_NOTE_UPDATE:		0x2B,

	// From server (40-7F)
	MSG_TYPE_AUTH_OK:				0x40,
	MSG_TYPE_AUTH_ERR:				0x41,
	MSG_TYPE_LOG_GAME_STEP:			0x50,
	MSG_TYPE_LOG_PROGRESS_TREE:		0x51,
	MSG_TYPE_LOG_MAP_DATA:			0x5A,
	LOG_TYPE_LOG_WAYPOINT_UPDATE:	0x5B,
	LOG_TYPE_LOG_WAYPOINT_DELETE:	0x5C,
	MSG_TYPE_LOG_MESSAGE:			0x60,
	MSG_TYPE_LOG_NOTE_UPDATE:		0x6A,
	MSG_TYPE_LOG_NOTE_DELETE:		0x6B
};

export function connectToServer() {
	const pid = localStorage.getItem("player-id");
	if (pid == null) {
		document.location = "login.html";
		return;
	}

	if (ws != null && ws.readyState == WebSocket.OPEN) {
		ws.onclose = null;
		ws.close();
	}

	ws = new WebSocket(server_addr);
	ws.onmessage = parseMessage;
	ws.onopen = () => ws.send(`${String.fromCharCode(Proto.MSG_TYPE_AUTH)}\u0000\u0000\u0000\u0000${pid}`);
	ws.onclose = () => {
		setTimeout(connectToServer, 3000);
		connected.update(v => false);
	};
}

export function tryAuth(player_id) {
	return new Promise((success, failed) => {
		const sock = new WebSocket(server_addr);
		sock.onmessage = (msg) => {
			sock.close();

			if (msg.data.charCodeAt(0) == Proto.MSG_TYPE_AUTH_OK)
				success();
			else
				failed();
		};
		sock.onclose = () => failed();
		sock.onopen = () => sock.send(`${String.fromCharCode(Proto.MSG_TYPE_AUTH)}\u0000\u0000\u0000\u0000${player_id}`);

		setTimeout(() => { try { sock.close() } catch(e) {} }, 5000);
	});
}

function parseMessage(msg) {
	switch (msg.data.charCodeAt(0)) {
		case Proto.MSG_TYPE_AUTH_OK: connected.update(v => true); break;
		case Proto.MSG_TYPE_AUTH_ERR: document.location = "login.html"; break;
		case Proto.MSG_TYPE_LOG_GAME_STEP: break;
		case Proto.MSG_TYPE_LOG_PROGRESS_TREE: break;
		case Proto.MSG_TYPE_LOG_MAP_DATA: break;
		case Proto.LOG_TYPE_LOG_WAYPOINT_UPDATE: break;
		case Proto.LOG_TYPE_LOG_WAYPOINT_DELETE: break;
		case Proto.MSG_TYPE_LOG_MESSAGE: break;
		case Proto.MSG_TYPE_LOG_NOTE_UPDATE: break;
		case Proto.MSG_TYPE_LOG_NOTE_DELETE: break;
	}
}

export default {};